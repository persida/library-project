package com.persidacvetkoska.libraryproject.country;

import com.persidacvetkoska.libraryproject.exception.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class CountryServiceTest {

    @Mock
    private CountryRepository countryRepository;

    @InjectMocks
    private CountryService countryService;

    @Test
    public void given_valid_country_id__when_find_by_id__then_return_country(){
        var country = new Country();
        when(countryRepository.findById(any())).thenReturn(Optional.of(new Country()));
        var result = countryService.findCountryById(0L);
        assertEquals(result, country.toDto());
    }

    @Test
    public void given_invalid_country_id__when_find_by_id__then_throw_exception(){
        when(countryRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> countryService.findCountryById(0L));
    }
}
