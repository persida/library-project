package com.persidacvetkoska.libraryproject.book;

import com.persidacvetkoska.libraryproject.author.Author;
import com.persidacvetkoska.libraryproject.author.AuthorRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BookServiceTest {

    @Mock
    private BookRepository bookRepository;

    @Mock
    private AuthorRepository authorRepository;

    @InjectMocks
    private BookService bookService;

    @Test
    public void given_valid_request__when_create_book__then_create(){
        var author = new Author();
        var bookRequest = new BookRequest("title", Instant.now(), );
        when(bookRepository.findByTitleAndAuthorId(any(), any())).thenReturn(Optional.empty());
        when(authorRepository.getOne(any())).thenReturn(author);

    }
}
