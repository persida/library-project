package com.persidacvetkoska.libraryproject.author;


import com.persidacvetkoska.libraryproject.country.Country;
import com.persidacvetkoska.libraryproject.country.CountryRepository;
import com.persidacvetkoska.libraryproject.exception.ResourceAlreadyExists;
import com.persidacvetkoska.libraryproject.exception.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.time.Instant;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AuthorServiceTest {

    @Mock
    private AuthorRepository authorRepository;

    @Mock
    private CountryRepository countryRepository;

    @InjectMocks
    private AuthorService authorService;

    @Test
    public void given_valid_create_request__when_create_author__then_create_author(){
        var authorCreateRequest = new AuthorCreateRequest("firstname", "lastname",
                Instant.parse("1970-11-30T18:35:24.00Z"), 1L);
        var country = new Country();
        when(authorRepository.findByFirstNameAndLastNameAndDateOfBirth(any(), any(), any()))
                .thenReturn(Optional.empty());
        when(countryRepository.findById(any()))
                .thenReturn(Optional.of(country));
        when(authorRepository.save(any(Author.class))).then(invocationOnMock -> invocationOnMock.getArgument(0, Author.class));

        var result = authorService.create(authorCreateRequest);
        var expectedResult = new AuthorDto(authorCreateRequest.firstName, authorCreateRequest.lastName,
                authorCreateRequest.dateOfBirth, country);
        assertNotNull(result);
        assertEquals(result, expectedResult);
    }

    @Test
    void given_author_exists__when_create_author__then_throw_exception(){
        when(authorRepository.findByFirstNameAndLastNameAndDateOfBirth(any(), any(), any()))
                .thenReturn(Optional.of(new Author()));
        assertThrows(ResourceAlreadyExists.class, () -> authorService.create(new AuthorCreateRequest()));
    }

    @Test
    void given_invalid_country__when_create_author__then_throw_exception(){
        when(authorRepository.findByFirstNameAndLastNameAndDateOfBirth(any(), any(), any()))
                .thenReturn(Optional.empty());
        when(countryRepository.findById(any()))
                .thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> authorService.create(new AuthorCreateRequest()));
    }

}
