package com.persidacvetkoska.libraryproject.config;

import com.persidacvetkoska.libraryproject.exception.ResourceNotFoundException;
import com.persidacvetkoska.libraryproject.utils.ErrorDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class.getName());

    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorDto handle(ResourceNotFoundException exception){
        LOGGER.error(exception.getMessage());
        return new ErrorDto(exception.getMessage());
    }

}
