package com.persidacvetkoska.libraryproject.utils;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ErrorDto {

    public String message;

}
