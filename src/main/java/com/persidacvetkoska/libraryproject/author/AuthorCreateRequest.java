package com.persidacvetkoska.libraryproject.author;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.time.Instant;

@AllArgsConstructor
@NoArgsConstructor
public class AuthorCreateRequest {
    @Size(max = 250, message = "First name should be up to 250 characters!")
    public String firstName;
    @Size(max = 250, message = "First name should be up to 250 characters!")
    public String lastName;
    @Past(message = "Date of birth must be in the past!")
    public Instant dateOfBirth;
    public Long countryId;
}
