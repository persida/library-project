package com.persidacvetkoska.libraryproject.author;

import org.springframework.data.jpa.domain.Specification;

public class AuthorSpecification {

    public static Specification<Author> bySearchPhrase(String searchPhrase){

        var searchPhraseToLower = '%' + searchPhrase.toLowerCase() + '%';
        return ((root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.or(
                        criteriaBuilder.like(criteriaBuilder.lower(root.get("firstName")), searchPhraseToLower),
                        criteriaBuilder.like(criteriaBuilder.lower(root.get("lastName")), searchPhraseToLower)
                ));
    }

}
