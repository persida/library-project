package com.persidacvetkoska.libraryproject.author;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;

@RestController
@RequestMapping("/api/authors")
public class AuthorResource {

    private final AuthorService authorService;

    public AuthorResource(AuthorService authorService){
        this.authorService = authorService;
    }


    @GetMapping(path = "/{id}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public AuthorDto findAuthorById(@PathVariable(value = "id") Long id){
        return authorService.findById(id);
    }

    @RolesAllowed("admin")
    @PostMapping(consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public AuthorDto create(@RequestBody AuthorCreateRequest request){
        return authorService.create(request);
    }

    @GetMapping(produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Page<AuthorDto> findPage(@RequestParam(name="authorName", required = false) final String authorName, Pageable pageable){
        return authorService.findPage(new AuthorSearchRequest(authorName, pageable));
    }


}
