package com.persidacvetkoska.libraryproject.author;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

@AllArgsConstructor
public class AuthorSearchRequest {

    public String authorName;
    public Pageable pageable;

    public Specification<Author> generateSpecification(){
        Specification<Author> specification = Specification.where(null);

        if(StringUtils.hasText(authorName)){
            specification = specification.and(AuthorSpecification.bySearchPhrase(authorName));
        }

        return specification;
    }
}
