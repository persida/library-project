package com.persidacvetkoska.libraryproject.author;

import com.persidacvetkoska.libraryproject.country.CountryRepository;
import com.persidacvetkoska.libraryproject.exception.ResourceAlreadyExists;
import com.persidacvetkoska.libraryproject.exception.ResourceNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AuthorService {

    private final AuthorRepository authorRepository;
    private final CountryRepository countryRepository;

    public AuthorDto findById(Long id){
        return authorRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Author with id: " + id + " not found!"))
                .toDto();
    }

    public Page<AuthorDto> findPage(AuthorSearchRequest searchRequest){
        return authorRepository.findAll(searchRequest.generateSpecification(), searchRequest.pageable)
                .map(Author::toDto);
    }

    public AuthorDto create(AuthorCreateRequest request){
        authorRepository.findByFirstNameAndLastNameAndDateOfBirth(request.firstName, request.lastName, request.dateOfBirth)
                .ifPresent(item -> {
                    throw new ResourceAlreadyExists("This author is already entered!");
                });
        var country = countryRepository.findById(request.countryId)
                .orElseThrow(() -> new ResourceNotFoundException("Invalid country!"));
        var author = new Author(request.firstName, request.lastName, request.dateOfBirth, country);
        return authorRepository.save(author).toDto();
    }

}
