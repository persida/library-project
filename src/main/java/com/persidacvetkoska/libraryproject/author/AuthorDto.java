package com.persidacvetkoska.libraryproject.author;

import com.persidacvetkoska.libraryproject.country.Country;
import lombok.AllArgsConstructor;
import lombok.ToString;

import java.time.Instant;
import java.util.Objects;

@AllArgsConstructor
@ToString
public class AuthorDto {
        public Long id;
        public String firstName;
        public String lastName;
        public Instant dateOfBirth;
        public Country country;

        public AuthorDto(String firstName, String lastName, Instant dateOfBirth, Country country) {
                this.firstName = firstName;
                this.lastName = lastName;
                this.dateOfBirth = dateOfBirth;
                this.country = country;
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                AuthorDto authorDto = (AuthorDto) o;
                return Objects.equals(id, authorDto.id) && firstName.equals(authorDto.firstName) && lastName.equals(authorDto.lastName) && dateOfBirth.equals(authorDto.dateOfBirth) && country.equals(authorDto.country);
        }

        @Override
        public int hashCode() {
                return Objects.hash(id, firstName, lastName, dateOfBirth, country);
        }
}
