package com.persidacvetkoska.libraryproject.author;

import com.persidacvetkoska.libraryproject.country.Country;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;
import java.util.Objects;

@Entity(name = "authors")
@AllArgsConstructor
@NoArgsConstructor
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "author_generator")
    @SequenceGenerator(name = "author_generator", sequenceName = "library.authors_id_seq", allocationSize=1)
    public Long id;
    @Column(name = "first_name")
    public String firstName;
    @Column(name = "last_name")
    public String lastName;
    @Column(name = "date_of_birth")
    public Instant dateOfBirth;
    @ManyToOne
    @JoinColumn(name = "country_id")
    public Country country;

    public Author(String firstName, String lastName, Instant dateOfBirth, Country country){
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.country = country;
    }

    public AuthorDto toDto(){
        return new AuthorDto(this.id, this.firstName, this.lastName, this.dateOfBirth, this.country);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Author author = (Author) o;
        return Objects.equals(id, author.id) && Objects.equals(firstName, author.firstName) && Objects.equals(lastName, author.lastName) && Objects.equals(dateOfBirth, author.dateOfBirth) && Objects.equals(country, author.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(31);
    }
}
