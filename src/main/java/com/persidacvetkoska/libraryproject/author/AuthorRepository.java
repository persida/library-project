package com.persidacvetkoska.libraryproject.author;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.time.Instant;
import java.util.Optional;

public interface AuthorRepository extends JpaRepository<Author, Long> , JpaSpecificationExecutor<Author> {

    Optional<Author> findByFirstNameAndLastNameAndDateOfBirth(String firstName, String lastName, Instant dateOfBirth);

}
