package com.persidacvetkoska.libraryproject.country;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Getter
public class CountryDto {
    Long id;
    String country;
    String countryCode;
}
