package com.persidacvetkoska.libraryproject.country;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/countries")
public class CountryResource {

    private CountryService countryService;

    public CountryResource(CountryService countryService){
        this.countryService = countryService;
    }

    @GetMapping(path = "/{id}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public CountryDto findCountryById(@PathVariable(value = "id") Long id){
        return countryService.findCountryById(id);
    }

    @GetMapping(path = "/find-all", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<CountryDto> findAll(){
        return countryService.findAll();
    }

    @GetMapping(produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Page<CountryDto> findPage(@RequestParam(name="country", required = false) String country,
                                     @RequestParam(name="countryCode", required = false) String countryCode, Pageable pageable){
        return countryService.findPage(new CountrySearchRequest(country, countryCode, pageable));
    }
}
