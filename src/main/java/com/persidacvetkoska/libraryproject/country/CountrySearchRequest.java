package com.persidacvetkoska.libraryproject.country;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import javax.swing.text.html.HTMLDocument;

@AllArgsConstructor
public class CountrySearchRequest {

    public String country;
    public String countryCode;
    public Pageable pageable;

    public Specification<Country> generateSpecification(){

        Specification<Country> specification = Specification.where(null);

        if(StringUtils.hasText(country)){
            specification = specification.and(CountrySpecification.byCountry(country));
        }

        if(StringUtils.hasText(countryCode)){
            specification = specification.and(CountrySpecification.byCountryCode(countryCode));
        }

        return specification;
    }
}
