package com.persidacvetkoska.libraryproject.country;

import lombok.Getter;

import javax.persistence.*;
import java.util.Objects;

@Entity(name="countries")
@Table(name = "countries")
@Getter
public class Country {
    @Id
    @GeneratedValue
    Long id;
    @Column
    String country;
    @Column
    String countryCode;

    public CountryDto toDto(){
        return new CountryDto(id, country, countryCode);
    }

}
