package com.persidacvetkoska.libraryproject.country;

import org.springframework.data.jpa.domain.Specification;

public class CountrySpecification {

    public static Specification<Country> byCountry(String country){

        var countrySearchString = '%' + country.toLowerCase() + '%';

        return ((root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.like(criteriaBuilder.lower(root.get("country")), countrySearchString));
    }

    public static Specification<Country> byCountryCode(String countryCode){
        var countryCodeSearchString = '%' + countryCode.toLowerCase() + '%';

        return ((root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.like(criteriaBuilder.lower(root.get("countryCode")), countryCodeSearchString));
    }
}
