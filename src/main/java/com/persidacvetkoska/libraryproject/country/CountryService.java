package com.persidacvetkoska.libraryproject.country;

import com.persidacvetkoska.libraryproject.exception.ResourceNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class CountryService {

    private CountryRepository countryRepository;

    public CountryDto findCountryById(Long id){

        return countryRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Country with id: " + id + " not found!"))
                .toDto();
    }

    public List<CountryDto> findAll(){
        return countryRepository.findAll().stream()
                .map(country -> country.toDto())
                .collect(Collectors.toList());
    }

    public Page<CountryDto> findPage(CountrySearchRequest searchRequest){
        return countryRepository.findAll(searchRequest.generateSpecification(), searchRequest.pageable)
                .map(Country::toDto);
    }
}
