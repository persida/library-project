package com.persidacvetkoska.libraryproject.book;


import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

@AllArgsConstructor
public class BookSearchRequest {

    public String title;
    public String authorName;
    public Pageable pageable;

    public Specification<Book> generateSpecification(){
        Specification<Book> bookSpecification = Specification.where(null);

        if (StringUtils.hasText(title)){
            bookSpecification = bookSpecification.and(BookSpecification.bySearchPhrase(title));
        }

        if (StringUtils.hasText(authorName)){
            bookSpecification = bookSpecification.and(BookSpecification.bySearchPhrase(authorName));
        }

        return bookSpecification;
    }

}
