package com.persidacvetkoska.libraryproject.book;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.math.BigDecimal;
import java.time.Instant;

public class BookRequest {
    @Length(max = 250, message = "Title length should be up to 250 characters!")
    @NotBlank(message = "Title is required!")
    public String title;
    @Past(message = "Date of publishing should be in the past!")
    @NotNull(message = "Date of publishing is required!")
    public Instant datePublish;
    @Length(max = 50, message = "ISBN length should be up to 250 characters!")
    @NotBlank(message = "ISBN is required!")
    public String isbn;
    @NotNull(message = "Price is required!")
    @Min(value = 0, message = "Price should be bigger or equal to 0!")
    public BigDecimal price;
    public Long authorId;

}
