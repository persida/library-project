package com.persidacvetkoska.libraryproject.book;

import com.persidacvetkoska.libraryproject.author.AuthorRepository;
import com.persidacvetkoska.libraryproject.exception.ResourceAlreadyExists;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class BookService {

    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;

    public BookDto create(final BookRequest request){

        var foundBook = bookRepository.findByTitleAndAuthorId(request.title, request.authorId);
        if (foundBook.isPresent()){
            throw new ResourceAlreadyExists("Book already exists!");
        }
        var author = authorRepository.getOne(request.authorId);
        var book = new Book(request.title, request.datePublish, request.isbn, request.price, author);
        return bookRepository.save(book).toDto();
    }

    public Page<BookDto> findPage(BookSearchRequest searchRequest){
        return bookRepository.findAll(searchRequest.generateSpecification(), searchRequest.pageable)
                .map(Book::toDto);
    }
}
