package com.persidacvetkoska.libraryproject.book;

import com.persidacvetkoska.libraryproject.author.Author;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

@Entity
@Table(name = "books")
@AllArgsConstructor
@NoArgsConstructor
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "book_generator")
    @SequenceGenerator(name = "book_generator", sequenceName = "library.books_id_seq", allocationSize=1)
    public Long id;
    public String title;
    public Instant dateOfPublish;
    public String isbn;
    public BigDecimal price;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="author_id")
    public Author author;

    public Book(String title, Instant dateOfPublish, String isbn, BigDecimal price, Author author){
        this.title = title;
        this.dateOfPublish = dateOfPublish;
        this.isbn = isbn;
        this.price = price;
        this.author = author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book entity = (Book) o;
        return Objects.equals(id, entity.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(31);
    }

    public BookDto toDto(){
        var processedAuthor = author != null ? author.toDto() : null;
        return new BookDto(id, title, dateOfPublish, isbn, price, processedAuthor);
    }

}
