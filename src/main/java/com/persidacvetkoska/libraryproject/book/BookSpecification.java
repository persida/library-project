package com.persidacvetkoska.libraryproject.book;

import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

@NoArgsConstructor
public class BookSpecification {

    public static Specification<Book> bySearchPhrase(final String searchPhrase){
        var searchPhraseToLower = '%' + searchPhrase.toLowerCase() + '%';
        return (root, criteriaQuery, criteriaBuilder) ->
            criteriaBuilder.or(
                    criteriaBuilder.like(criteriaBuilder.lower(root.get("title")), searchPhraseToLower),
                    criteriaBuilder.like(criteriaBuilder.lower(root.get("author").get("firstName")), searchPhraseToLower),
                    criteriaBuilder.like(criteriaBuilder.lower(root.get("author").get("lastName")), searchPhraseToLower)
            );
        }

}
