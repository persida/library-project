package com.persidacvetkoska.libraryproject.book;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;
import javax.annotation.security.RolesAllowed;

@RestController
@RequestMapping("/api/books")
@RequiredArgsConstructor
public class BookResource {

    private final BookService bookService;

    @RolesAllowed("admin")
    @PostMapping(consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public BookDto create(@RequestBody final BookRequest request){
        return bookService.create(request);
    }

    @GetMapping(produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Page<BookDto> findPage(
            @RequestParam(value = "name", required = false) final String title,
            @RequestParam(value = "author", required = false) final String authorName, Pageable pageable){
        return bookService.findPage(new BookSearchRequest(title, authorName, pageable));
    }
}
