package com.persidacvetkoska.libraryproject.book;

import com.persidacvetkoska.libraryproject.author.AuthorDto;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.Instant;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class BookDto {
    public Long id;
    public String title;
    public Instant datePublish;
    public String isbn;
    public BigDecimal price;
    public AuthorDto author;
}
