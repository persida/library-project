CREATE TABLE books (
    id bigserial NOT NULL,
    title character varying(250) NOT NULL,
    date_of_publish timestamp with time zone NOT NULL,
    isbn character varying(50) NOT NULL,
    price numeric(8, 2) NOT NULL,
    version bigint,
    author_id bigint,
    CONSTRAINT pk_book_id PRIMARY KEY (id),
    CONSTRAINT fk_author_id FOREIGN KEY(author_id)
            REFERENCES authors(id)
);
CREATE INDEX idx_books_author_id
ON books(author_id);