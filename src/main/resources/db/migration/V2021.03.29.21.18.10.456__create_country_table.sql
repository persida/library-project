CREATE TABLE countries (
    id bigserial NOT NULL,
    country character varying(250) NOT NULL,
    country_code character varying(3) NOT NULL,
    CONSTRAINT pk_countries_id PRIMARY KEY (id)
);