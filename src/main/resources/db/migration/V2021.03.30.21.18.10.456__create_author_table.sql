CREATE TABLE authors (
    id bigserial NOT NULL,
    first_name character varying(250) NOT NULL,
    last_name character varying(250) NOT NULL,
    date_of_birth timestamp with time zone NOT NULL,
    country_id bigint NOT NULL,
    CONSTRAINT pk_author_id PRIMARY KEY (id),
    CONSTRAINT fk_country_id FOREIGN KEY(country_id)
                REFERENCES countries(id)
);
CREATE INDEX idx_authors_country_id
ON authors(country_id);